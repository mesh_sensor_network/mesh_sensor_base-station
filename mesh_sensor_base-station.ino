// rf22_mesh_server1.pde
// -*- mode: C++ -*-
// Example sketch showing how to create a simple addressed, routed reliable messaging server
// with the RHMesh class.
// It is designed to work with the other examples rf22_mesh_*
// Hint: you can simulate other network topologies by setting the 
// RH_TEST_NETWORK define in RHRouter.h

// Mesh has much greater memory requirements, and you may need to limit the
// max message length to prevent wierd crashes
#define RH_MESH_MAX_MESSAGE_LEN 50

#include <RHMesh.h>
#include <RH_RF22.h>
#include <SPI.h>

// In this small artifical network of 4 nodes,
#define CLIENT_ADDRESS 1
#define SERVER1_ADDRESS 2
#define SERVER2_ADDRESS 3
#define SERVER3_ADDRESS 4

// Singleton instance of the radio driver
RH_RF22 driver(10, A5);

// Class to manage message delivery and receipt, using the driver declared above
RHMesh manager(driver, SERVER3_ADDRESS);

void setup() 
{
  Serial.begin(115200);
  if (!manager.init())
    Serial.println("RF22 init failed");
  // Defaults after init are 434.0MHz, 0.05MHz AFC pull-in, modulation FSK_Rb2_4Fd36
}

union Data {
  float a;
  uint8_t bytes[4];
};

// Dont put this on the stack:
uint8_t buf[RH_MESH_MAX_MESSAGE_LEN];
void loop()
{
  uint8_t len = sizeof(buf);
  uint8_t from;

  union Data temp, humidity, uvLvl, batLvl;
  uint16_t messageNum = 0;
  
  if (manager.recvfromAck(buf, &len, &from))
  {
    Serial.print("got request from : 0x");
    Serial.print(from, HEX);
    Serial.println(": ");

    
    for(int i = 0; i < 16; i++) {
      Serial.print(buf[i], HEX);
      Serial.print(":");
    }
    Serial.println();
    

    memcpy(&messageNum, &buf[0], sizeof(messageNum));
    memcpy(&temp.bytes, &buf[2], sizeof(temp.bytes));
    memcpy(&humidity.bytes, &buf[6], sizeof(humidity.bytes));
    memcpy(&uvLvl.bytes, &buf[10], sizeof(uvLvl.bytes));
    memcpy(&batLvl.bytes, &buf[14], sizeof(batLvl.bytes));

    Serial.print("\tmessage num: ");
    Serial.println(messageNum);
    Serial.print("\ttemp: ");
    Serial.println(temp.a);
    Serial.print("\thumidity: ");
    Serial.println(humidity.a);
    Serial.print("\tuv lvl: ");
    Serial.println(uvLvl.a);
    Serial.print("\tbat lvl: ");
    Serial.println(batLvl.a);
    Serial.print("\trssi: ");
    Serial.println(driver.lastRssi());
    Serial.println();
  }
}

